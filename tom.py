#!/usr/bin/env python3

import itertools
import collections
import random
import statistics
import string

import colorama

import wordle

def is_possible(answer, greens, greys, yellows):
  return (
    all((letter == answer[index]) for (index, letter) in greens) and
    all((letter not in answer) for letter in greys) and
    all(((letter in answer) and (letter != answer[index])) for (index, letter) in yellows)
  )

ELIMINATIONS_GREEN = {}
def eliminations_green():
  if ELIMINATIONS_GREEN:
    return ELIMINATIONS_GREEN
  for index, letter in itertools.product(range(5), string.ascii_lowercase):
    ELIMINATIONS_GREEN[index, letter] = set()
    for answer in wordle.answers():
      if letter != answer[index]:
        ELIMINATIONS_GREEN[index, letter].add(answer)
  return ELIMINATIONS_GREEN

ELIMINATIONS_GREY = {}
def eliminations_grey():
  if ELIMINATIONS_GREY:
    return ELIMINATIONS_GREY
  for letter in string.ascii_lowercase:
    ELIMINATIONS_GREY[letter] = set()
    for answer in wordle.answers():
      if letter not in answer:
        ELIMINATIONS_GREY[letter].add(answer)
  return ELIMINATIONS_GREY

ELIMINATIONS_YELLOW = {}
def eliminations_yellow():
  if ELIMINATIONS_YELLOW:
    return ELIMINATIONS_YELLOW
  for index, letter in itertools.product(range(5), string.ascii_lowercase):
    ELIMINATIONS_YELLOW[index, letter] = set()
    for answer in wordle.answers():
      if (letter == answer[index]) or (letter in answer):
        ELIMINATIONS_YELLOW[index, letter].add(answer)
  return ELIMINATIONS_YELLOW

ELIMINATIONS = {}
def eliminations():
  global ELIMINATIONS
  if ELIMINATIONS:
    return ELIMINATIONS
  answers = wordle.answers()
  guesses = wordle.guesses()
  for answer in answers:
    print("answer", answer)
    ELIMINATIONS[answer] = {}
    answer_letters = set(answer)
    for guess in guesses:
      eliminated = set()
      for index, (guess_letter, answer_letter) in enumerate(zip(guess, answer)):
        if guess_letter == answer_letter:
          eliminated.update(eliminations_green()[index, guess_letter])
        elif guess_letter in answer_letters:
          eliminated.update(eliminations_yellow()[index, guess_letter])
        else:
          eliminated.update(eliminations_grey()[guess_letter])
      ELIMINATIONS[answer][guess] = eliminated

      # print("guess", guess)
      # greens = tuple((index, letter) for index, letter in enumerate(guess) if (letter == answer[index]))
      # greys = tuple(letter for letter in guess if (letter not in answer))
      # yellows = tuple((index, letter) for index, letter in enumerate(guess) if ((letter in answer) and (letter != answer[index])))
      # print("greens", greens)
      # print("greys", greys)
      # print("yellows", yellows)
      # ELIMINATIONS[answer][guess] = set(answer for answer in answers if not is_possible(answer, greens, greys, yellows))
  return ELIMINATIONS

def select_guess_eliminations(possible_answers, possible_guesses):
  global ELIMINATIONS
  eliminations()
  best_guess = None
  best_guess_score = None
  for guess in possible_guesses:
    guess_score = 1 if (guess in possible_answers) else 0
    for answer in possible_answers:
      guess_score += len(ELIMINATIONS[answer][guess] & possible_answers)
    if best_guess is None or guess_score > best_guess_score:
      best_guess = word
      best_guess_score = guess_score
  return best_guess

def select_guess_frequencies(possible_answers, possible_guesses):
  letter_frequencies = collections.defaultdict(lambda: 0)
  letter_index_frequencies = collections.defaultdict(lambda: 0)
  for word in possible_answers:
    for letter in set(word):
      letter_frequencies[letter] += 1
    for index, letter in enumerate(word):
      letter_index_frequencies[index, letter] += 1

  letter_frequency_median = len(possible_answers) // 2
  letter_index_frequency_median = len(possible_answers) // 2

  for letter in letter_frequencies.keys():
    letter_frequencies[letter] = max(0, letter_frequency_median - abs(letter_frequencies[letter] - letter_frequency_median))
  for index, letter in letter_index_frequencies.keys():
    letter_index_frequencies[index, letter] = max(0, letter_index_frequency_median - abs(letter_index_frequencies[index, letter] - letter_index_frequency_median))

  is_possible_answer_bonus = max(0, 4 - len(possible_answers))

  best_guess = None
  best_guess_score = None
  for word in possible_guesses:
    guess_score = (
      sum(letter_frequencies[letter] for letter in set(word)) +
      sum(letter_index_frequencies[index, letter] for (index, letter) in enumerate(word)) +
      (is_possible_answer_bonus if word in possible_answers else 0)
    )
    # if word in possible_answers or word in {"dumky", "evoke", "kemps", "kendo", "mikva", "padma", "vodka"}:
    #   print(
    #     word,
    #     guess_score,
    #     tuple(letter_frequencies[letter] for letter in word),
    #     tuple(letter_index_frequencies[index, letter] for (index, letter) in enumerate(word)),
    #     (is_possible_answer_bonus if word in possible_answers else 0)
    #   )
    if best_guess is None or guess_score > best_guess_score:
      best_guess = word
      best_guess_score = guess_score

  return best_guess

def select_guess(possible_answers, possible_guesses):
  # return select_guess_eliminations(possible_answers, possible_guesses)
  return select_guess_frequencies(possible_answers, possible_guesses)

@wordle.solver("tom")
def main(game):
  possible_answers = set(wordle.answers())
  possible_guesses = set(wordle.guesses())
  while not game.correct:

    # print(list(sorted(possible_answers))[:8])

    guess = None
    if game.guesses == 0:
      guess = "soare"
    elif len(possible_answers) == 1:
      guess = next(iter(possible_answers))
    else:
      guess = select_guess(possible_answers, possible_guesses)

    response = game.guess(guess)
    for index, (letter, colour) in enumerate(zip(guess, response)):
      # print(wordle.COLORAMA[colour] + letter, end="")
      if colour == wordle.GREEN:
        possible_answers.difference_update(tuple(filter(
          lambda word: word[index] != letter,
          possible_answers
        )))
      elif colour == wordle.GREY:
        possible_answers.difference_update(tuple(filter(
          lambda word: letter in word,
          possible_answers
        )))
      elif colour == wordle.YELLOW:
        possible_answers.difference_update(tuple(filter(
          lambda word: (letter not in word) or (word[index] == letter),
          possible_answers
        )))
    # print(colorama.Style.RESET_ALL)

if __name__ == "__main__":
  wordle.main()
