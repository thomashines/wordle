#!/usr/bin/env python3

import random
import time

import colorama

GREEN = 0
GREY = 1
YELLOW = 2
COLORAMA = {
  GREEN: colorama.Fore.GREEN,
  GREY: colorama.Fore.WHITE,
  YELLOW: colorama.Fore.YELLOW,
}

ANSWERS = ()
def answers():
  global ANSWERS
  if ANSWERS:
    return ANSWERS
  answers = set()
  with open("answers.txt", "r") as answers_file:
    for answer in answers_file:
      answers.add(answer.strip())
  ANSWERS = tuple(sorted(answers))
  return ANSWERS

GUESSES = ()
def guesses():
  global GUESSES
  if GUESSES:
    return GUESSES
  guesses = set(answers())
  with open("guesses.txt", "r") as guesses_file:
    for guess in guesses_file:
      guesses.add(guess.strip())
  GUESSES = tuple(sorted(guesses))
  return GUESSES

SOLVERS = []
def solver(name):
  def _solver(func):
    global SOLVERS
    SOLVERS.append((name, func))
    return func
  return _solver

class Game:
  def __init__(self, word):
    self.word = word
    self.guesses = 0
    self.correct = False
  def guess(self, check):
    self.guesses += 1
    self.correct = (check == self.word)
    response = []
    for index, letter in enumerate(check):
      if self.word[index] == letter:
        response.append(GREEN)
      elif letter in self.word:
        response.append(YELLOW)
      else:
        response.append(GREY)
    return response

TRIALS = 64
def main():
  global SOLVERS
  print("trials", TRIALS)
  for name, solver in SOLVERS:
    total_guesses = 0
    trial_answers = set(answers())
    for trial in range(1, TRIALS + 1):
      answer = random.choice(tuple(trial_answers))
      trial_answers.remove(answer)
      game = Game(answer)
      solver(game)
      # print(name, game.word, game.correct, game.guesses)
      total_guesses += game.guesses
    print(name, "total_guesses", total_guesses)
    print(name, "average", total_guesses / TRIALS)

if __name__ == "__main__":
  main()
